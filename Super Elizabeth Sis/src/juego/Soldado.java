package juego;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Soldado {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	private Image img;

	public Soldado(double x, double y, double ancho, double alto) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = 1.2;
		this.img = Herramientas.cargarImagen("soldado.png");
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getAncho() {
		return this.ancho;
	}

	public double getAlto() {
		return this.alto;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(this.img, this.x, this.y, 0, 0.255);
	}

	public void mover() {
		x -= this.velocidad;
	}

	public boolean chocasteConEntorno(Entorno entorno) { // Devuelve true si el lado derecho del soldado quedó fuera del entorno
		return this.x + this.ancho / 2 < 0;
	}

	public boolean chocasteConFuego(Fuego fuego) { // Chequeo si el lado de abajo o derecho del fuego entro en contacto con el soldado
		return (fuego.getX() + fuego.getDiametro() / 2 >= this.x - this.ancho / 2
				&& fuego.getX() + fuego.getDiametro() / 2 <= this.getX() + this.getAncho() / 2)
				&& (fuego.getY() + fuego.getDiametro() / 2 >= this.getY() - this.alto / 2
						&& fuego.getY() + fuego.getDiametro() / 2 <= this.y + this.alto / 2);
	}


	public void reaparecer(Soldado[] s, Entorno entorno) {// asigna un x especificica al objeto para evitar que se
															// superponga con otro del mismo tipo
		double Xmax=entorno.ancho();
		for (int i = 0; i < s.length; i++) {
			if(s[i]!=null && s[i].getX()>Xmax) {
				Xmax=s[i].getX();
			}
		}
		this.x = Xmax + 300 + 100 * Math.random();
	}
}
