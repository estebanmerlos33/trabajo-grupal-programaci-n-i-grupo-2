package juego;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Fuego {
	private double x;
	private double y;
	private double diametro;
	private double velocidad;
	private Image imagen;

	public Fuego(double x, double y, double diametro) {
		this.x = x;
		this.y = y;
		this.diametro = diametro;
		this.velocidad = 2;
		this.imagen = Herramientas.cargarImagen("bolaDeFuego.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(this.imagen, this.x, this.y, 0, 0.025);
	}

	public void mover() {
		x += velocidad;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public double getDiametro() {
		return this.diametro;
	}

	public boolean chocasteconborde(Entorno entorno) {
		if (x + this.diametro / 2 < 0 || x - this.diametro / 2 > entorno.ancho()) {
			return true;
		}
		return false;
	}

	public boolean chocasteConSoldado(Soldado s) {
		return (this.x + this.diametro / 2 >= s.getX() - s.getAncho() / 2
				&& this.x + this.diametro / 2 <= s.getX() + s.getAncho() / 2)
				&& this.y + this.diametro / 2 >= s.getY() - s.getAlto() / 2;
	}

	public boolean chocasteConObstaculo(Obstaculo o) {
		if ((this.x + this.diametro / 2 > o.getX() - o.getAncho() / 2
				&& this.x + this.diametro / 2 < o.getX() + o.getAncho() / 2)
				&& this.y + this.diametro / 2 > o.getY() - o.getAlto() / 2) {
			return true;
		}
		return false;
	}

}
